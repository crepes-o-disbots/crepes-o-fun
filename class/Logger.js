/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const pino = require('pino');

/**
 * Logger
 */
class Logger {
    constructor() {
        this.logger = null;
    }

    /**
     * Creates the logger for the program
     */
    createLogger() {
        // Create a silent logger, no folder creation, no file, nothing.
        if (process.env.LOGS === 'silent') {
            this.logger = pino({
                level: 'silent',
            });
        }
        // Else, create a normal logger
        else {
            // Get a date object, create the base for the file name
            const currDateTime = new Date;
            let logFileName = currDateTime.getFullYear() + '-' + (currDateTime.getMonth() + 1) + '-' + currDateTime.getDate() + '-';

            // Loop, we need to get the file name to use for the log file
            let cnt = 1;
            while (require('fs').existsSync('./logs/' + logFileName + cnt + '.json')) {
                cnt++;
            }
            logFileName += cnt;

            // No files
            if (process.env.LOGS === 'nofile') {
                this.logger = pino({
                    level: process.env.NODE_ENV === 'development' ? 'debug' : 'info', // Allow debug informations on dev env
                    transport : {
                        // Format using pino-pretty
                        target: 'pino-pretty',
                        options: {
                            colorize: require('colorette').isColorSupported, // Enable only if the terminal supports it
                            translateTime: 'SYS:dd/mm/yyyy HH:MM:ss', // --translateTime
                            ignore: 'pid,hostname', // --ignore
                        },
                    },
                });
            }
            // Assign a normal logger
            else {
                this.logger = pino({
                    level: process.env.NODE_ENV === 'development' ? 'debug' : 'info', // Allow debug information on dev env
                    transport : {
                        // Two outputs, one in a file, the other on execution
                        targets: [
                            // Format using pino-pretty
                            {
                                target: 'pino-pretty',
                                options: {
                                    colorize: require('colorette').isColorSupported, // Enable only if the terminal supports it
                                    translateTime: 'SYS:dd/mm/yyyy HH:MM:ss', // --translateTime
                                    ignore: 'pid,hostname', // --ignore
                                },
                            },
                            {
                                target: 'pino/file',
                                options: {
                                    destination: './logs/' + logFileName + '.json',
                                    minLength: 512, // Buffer before writing
                                    sync: false, // Asynchronous logging, default value
                                    mkdir: true,
                                },
                            },
                        ],
                    },
                });
            }
        }
    }
}

// Singleton object
const loggerInstance = new Logger();

// Export the logger
module.exports = loggerInstance;