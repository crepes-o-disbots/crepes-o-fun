/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Class defining a component.
 */
class Component {
    constructor(options = {
        // Component name
        name : undefined,

        // Description, what does it do
        description : undefined,

        // Disable globally, if this is a command, it won't be sent to discord's API
        disabled : undefined,

        // Used to categorize commands specifically, for a "/help" command
        category : undefined,

        // Required intents for the component to work as intended. See GatewayIntentBits doc for input choices
        intents : [],

        // Component infos to send to discord's API, should use a SlashCommandBuilder or ButtonBuilder or anything else that can be built. This is also used to define what this component is
        builder : undefined,

        // Function to execute when user is entering a command, replying with autocomplete information
        autocompleteFunction : undefined,

        // Function to execute when this component is called
        mainFunction : undefined,

        // Cooldown of the component, in seconds
        cooldown : 0,
    }) {
        this.name = options.name;
        this.description = options.description;
        this.disabled = options.disabled;
        this.category = options.category;
        this.intents = options.intents;
        this.builder = options.builder;
        this.autocompleteFunction = options.autocompleteFunction;
        this.mainFunction = options.mainFunction;
        this.cooldown = options.cooldown;
    }
}

/**
 * Parse full path & name & extension of js file, leaving only the name
 * @param fileName
 * File name, usually __filename
 * @returns {string}
 * Lowercase name, for example : /home/user/Test.js => Test
 */
function parseName(fileName) {
    return fileName.split('\\').pop().split('/').pop().split('.js')[0];
}

/**
 * Parse full path & name & extension of js file, leaving only the lowercase name
 * @param fileName
 * File name, usually __filename
 * @returns {string}
 * Lowercase name, for example : /home/user/Test.js => test
 */
function parseNameToLower(fileName) {
    return parseName(fileName).toLowerCase();
}

/**
 * Answers an autocomplete, when options are set by the user
 * This should not be used if you need to give users snowflakes of something like that
 *
 * @param interaction
 * The interaction to answer to
 * @param optionsMap
 * The options available to the user
 */
async function autocompleteAnswer(interaction, optionsMap) {
    // Get the option to give the correct autocomplete information, getFocussed doesn't work as intended.
    const focusedOption = interaction.options._hoistedOptions[0];

    // Choices available to the user with this answer
    const choices = optionsMap.get(focusedOption.name);

    // The user may have entered something, we filter based on that
    const filter = choices.filter(choice => choice.startsWith(focusedOption.value));
    await interaction.respond(
        filter.map(choice => ({ name: choice, value: choice })),
    );
}

// Export class
module.exports = { Component, parseName, parseNameToLower, autocompleteAnswer };