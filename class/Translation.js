/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Linguini, TypeMappers } = require('linguini');

/**
 * Translation
 */
class Translation {
    constructor() {
        this.translator = new Linguini(require('path').join(__dirname, '../lang'), 'lang');
        if (process.env.LANGUAGE === undefined) {
            process.env.LANGUAGE = 'en-US';
        }
        if (this.translator.langDatas['en-US'] == null) {
            console.error('Missing english translation !\nThis is required as a fallback in case of any other missing translation, please add it.\nExiting.');
            process.exit(1);
        }
    }

    /**
     * Returns the correct translation element.
     * You shouldn't use this as the getxxx methods are here for that
     * Uses Linguini's get method
     * @see Linguini
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     *
     * @param langKey
     * Language key, usually 'en' or 'fr' depending on what you need
     *
     * @param mapper
     * TypeMapper requested
     *
     * @param variables
     * Json array containing variables for the translation to replace. Every element given must have the same variable name as in the translation
     *
     * @returns {*}
     * Either the translated element in the required language, or in english as a fallback
     */
    get(key, langKey, mapper, variables) {
        // Load logger after it is initialized
        const { logger } = require('./Logger');
        try {
            return this.translator.get(key, langKey, mapper, variables);
        }
        catch (err) {
            try {
                logger.warn(`Detected missing translation for key ${key} with langKey ${langKey}`);
                return this.translator.get(key, 'en-US', mapper, variables);
            }
            catch (anErr) {
                logger.error(`Unable to fallback to english for translation ${key}.`);
                return '';
            }
        }
    }

    /**
     * Returns associated translation string
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     *
     * @param langKey
     * Language key, usually 'en' or 'fr' depending on what you need
     *
     * @param variables
     * Json array containing variables for the translation to replace. Every element given must have the same variable name as in the translation
     *
     * @returns {*}
     * Either the translated string in the required language, or in english as a fallback
     */
    getString(key, variables = null, langKey = process.env.LANGUAGE) {
        return this.get(key, langKey, TypeMappers.String, variables);
    }

    /**
     * Returns associated translation boolean
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     *
     * @param langKey
     * Language key, usually 'en' or 'fr' depending on what you need
     *
     * @param variables
     * Json array containing variables for the translation to replace. Every element given must have the same variable name as in the translation
     *
     * @returns {*}
     * Either the translated boolean in the required language, or from the english translation as a fallback
     */
    getBool(key, variables = null, langKey = process.env.LANGUAGE) {
        return this.get(key, langKey, TypeMappers.Boolean, variables);
    }

    /**
     * Returns associated translation number
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     *
     * @param langKey
     * Language key, usually 'en' or 'fr' depending on what you need
     *
     * @param variables
     * Json array containing variables for the translation to replace. Every element given must have the same variable name as in the translation
     *
     * @returns {*}
     * Either the translated number in the required language, or from the english translation as a fallback
     */
    getNumber(key, variables = null, langKey = process.env.LANGUAGE) {
        return this.get(key, langKey, TypeMappers.Number, variables);
    }

    /**
     * Returns associated translation date
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     *
     * @param langKey
     * Language key, usually 'en' or 'fr' depending on what you need
     *
     * @param variables
     * Json array containing variables for the translation to replace. Every element given must have the same variable name as in the translation
     *
     * @returns {*}
     * Either the translated date in the required language, or from the english translation as a fallback
     */
    getDate(key, variables = null, langKey = process.env.LANGUAGE) {
        return this.get(key, langKey, TypeMappers.Date, variables);
    }

    /**
     * Returns a JsonArray containing the requested translations
     * Should the translation be missing, it won't be added to the returned array
     *
     * @param key
     * Translation key string, may be 'core.test' or something else
     */
    getAllTranslations(key) {
        const res = {};
        for (const i in this.translator.langDatas) {
            if (this.translator.langDatas[i].data[key]) {
                res[i] = this.translator.langDatas[i].data[key];
            }
        }
        return res;
    }
}

// Singleton object
const translatorInstance = new Translation();

// Export the logger
module.exports = translatorInstance;
