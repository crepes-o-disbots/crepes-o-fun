'use strict';
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('EggTryStats', {
            userId: {
                primaryKey: true,
                comment: 'User snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            serverId: { primaryKey: true,
                comment: 'Server snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            win: {
                comment: 'Number of time the user got the role',
                type: Sequelize.INTEGER,
                defaultValue: 0,
                allowNull: false,
            },
            fail: {
                comment: 'Number of time the user failed to get the role',
                type: Sequelize.INTEGER,
                defaultValue: 0,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('EggTryStats');
    },
};