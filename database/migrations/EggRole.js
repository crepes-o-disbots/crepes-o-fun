'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('EggRoles', {
            serverId: {
                primaryKey: true,
                comment: 'Server snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            role: {
                primaryKey: true,
                comment: 'Role snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            waitingTime: {
                comment: 'Time to wait before taking the egg role again on this server',
                type: Sequelize.DATE,
                defaultValue: null,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('EggRoles');
    },
};
