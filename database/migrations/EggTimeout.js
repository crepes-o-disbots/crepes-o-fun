'use strict';

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('EggTimeouts', {
            serverId: {
                primaryKey: true,
                comment: 'Server snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            userId: {
                primaryKey: true,
                comment: 'User snowflake (id), given by discord',
                type: Sequelize.STRING,
            },
            expiresOn: {
                comment: 'When the user can be removed from this table',
                type: Sequelize.DATE,
                defaultValue: null,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('EggTimeouts');
    },
};
