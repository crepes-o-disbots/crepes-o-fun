'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class EggTimeout extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            // define association here
        }
    }
    EggTimeout.init({
        serverId: {
            primaryKey: true,
            comment: 'Server snowflake (id), given by discord',
            type: DataTypes.STRING, // Not ideal, but sequelize's return values are undefined if we use a BIGINT.
            get() {
                const value = this.getDataValue('serverId');
                return value ? value : null;
            },
        },
        userId: {
            primaryKey: true,
            comment: 'User snowflake (id), given by discord',
            type: DataTypes.STRING,
            get() {
                const value = this.getDataValue('userId');
                return value ? value : null;
            },
        },
        expiresOn: {
            comment: 'When the user can be removed from this table',
            type: DataTypes.DATE,
            defaultValue: null,
            get() {
                const value = this.getDataValue('expiresOn');
                return value ? value : null;
            },
        },
    }, {
        sequelize,
        modelName: 'EggTimeout',
    });
    return EggTimeout;
};
