'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class EggRole extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
        static associate(models) {
            // define association here
        }
    }
    EggRole.init({
        serverId: {
            primaryKey: true,
            comment: 'Server snowflake (id), given by discord',
            type: DataTypes.STRING, // Not ideal, but sequelize's return values are undefined if we use a BIGINT.
            get() {
                const value = this.getDataValue('serverId');
                return value ? value : null;
            },
        },
        role: {
            primaryKey: true,
            comment: 'Role snowflake (id), given by discord',
            type: DataTypes.STRING,
            get() {
                const value = this.getDataValue('role');
                return value ? value : null;
            },
        },
        waitingTime: {
            comment: 'Time to wait before taking the egg role again on this server',
            type: DataTypes.DATE,
            defaultValue: null,
            get() {
                const value = this.getDataValue('waitingTime');
                return value ? value : null;
            },
        },
    }, {
        sequelize,
        modelName: 'EggRole',
    });
    return EggRole;
};
