'use strict';
const {
    Model,
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class EggTryStats extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    }
    EggTryStats.init({
        userId: {
            primaryKey: true,
            comment: 'User snowflake (id), given by discord',
            type: DataTypes.STRING,
            get() {
                const value = this.getDataValue('userId');
                return value ? value : null;
            },
        },
        serverId: {
            primaryKey: true,
            comment: 'Server snowflake (id), given by discord',
            type: DataTypes.STRING,
            get() {
                const value = this.getDataValue('serverId');
                return value ? value : null;
            },
        },
        win: {
            comment: 'Number of time the user got the role',
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
        fail: {
            comment: 'Number of time the user failed to get the role',
            type: DataTypes.INTEGER,
            defaultValue: 0,
            allowNull: false,
        },
    }, {
        sequelize,
        modelName: 'EggTryStats',
    });
    return EggTryStats;
};