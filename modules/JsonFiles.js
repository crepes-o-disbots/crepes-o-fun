/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const fs = require('fs');

const confFolder = 'conf/';
const serverFolders = 'servers/';

/**
 * Creates a directory recursively using the given path
 *
 * Uses fs.mkdirSync
 * @see fs.mkdirSync
 *
 * @param path
 * The path of the directory to create
 *
 * @returns {boolean}
 * Returns if the directory was created or not
 * A wrong return value either means that the folder already existed or that fs.mkdirSync failed to create it.
 */

function createDir(path) {
    let res = false;

    if (!checkExistence(path)) {
        try {
            fs.mkdirSync(path, { recursive: true });
            res = true;
        }
        catch (err) {
            console.log(err);
        }
    }

    return res;
}

/**
 * Tries to create a json file using fs.writeFileSync
 * @see fs.writeFileSync
 *
 * @param path
 * The path of the file to create
 *
 * @return {boolean}
 * Returns if the file was created or not
 * A wrong return value either means that the file already existed or that fs.writeFileSync failed to create it.
 */
function createJsonFile(path) {
    let res = false;

    if (!checkExistence(path)) {
        try {
            fs.writeFileSync(path, '{}');
            res = true;
        }
        catch (err) {
            console.log(err);
        }
    }

    return res;
}

/**
 * Checks if a folder or file exists
 *
 * Uses fs.existsSync
 * @see fs.existsSync
 *
 * @param path
 * Path to use for the check
 *
 * @returns {boolean}
 * Returns if the folder or file was found
 */
function checkExistence(path) {
    return fs.existsSync(path);
}

/**
 * Checks if the config folder exists using the confFolder variable.
 *
 * Uses checkExistence
 * @see checkExistence
 *
 * @returns {boolean}
 * Returns if the mentioned folder was found
 */
function checkConfigFolder() {
    return checkExistence(confFolder);
}

/**
 * Checks if the server folder exists using the confFolder and serverFolders variables.
 *
 * Uses checkExistence
 * @see checkExistence
 *
 * @returns {boolean}
 * Returns if the mentioned folder was found
 *
 * @deprecated
 */
function checkServerFolder() {
    return checkExistence(confFolder + serverFolders);
}

/**
 * Checks if the guild's config folder exists using the confFolder and serverFolders variables and a given guildId
 *
 * Uses checkExistence
 * @see checkExistence
 *
 * @param guildId
 * A Twitter snowflake, that being the server's id
 *
 * @returns {boolean}
 * Returns if the mentioned folder was found
 *
 * @deprecated
 */
function checkGuildServerFolder(guildId) {
    return checkExistence(confFolder + serverFolders + guildId);
}

/**
 * Reads a json file using a path
 *
 * Uses fs.readFileSync and JSON.parse
 * @see fs.readFileSync
 * @see JSON.parse
 *
 * @param path
 * Path to the file
 *
 * @return {null|any}
 * Returns null in case of a reading error or a JSON parsed string
 */
function readJsonFile(path) {
    try {
        const jsonString = fs.readFileSync(path, ('utf-8'));
        return JSON.parse(jsonString);
    }
    catch (err) {
        console.log(err);
        return null;
    }
}

/**
 * Reads a json file using a guildId and a file name given
 *
 * Uses readJsonFile
 * @see readJsonFile
 *
 * @param guildId
 * A Twitter snowflake, that being the server's id
 *
 * @param conFile
 * The name of the json file to read without '.json'
 *
 * @return {*|null}
 * Json data, can be empty in case the function creates it or null in case the file doesn't exist
 *
 * @deprecated
 */
function readFile(guildId, conFile) {
    let toRead;
    if (!guildId) toRead = confFolder + conFile + '.json';
    else toRead = confFolder + serverFolders + guildId + '/' + conFile + '.json';


    let conf = readJsonFile(toRead);
    if (!conf) {
        console.log('Unable to read file "' + toRead + '" , please check your permissions or if the file exists.');
        conf = null;
    }

    return conf;
}

/**
 * Reads some data from a given config file.
 * May use a guildId for servers specific configurations.
 *
 * Uses readFile
 * @see readFile
 *
 * @param guildId
 * A Twitter snowflake, that being the server's id
 *
 * @param conFile
 * The name of the json file to read without '.json'
 *
 * @param data
 * The data to read, must exist in the json file
 *
 * @return {null|*}
 * Null in case the config file doesn't exist or doesn't contain the requested data. In case of a successful operation, the data asked
 *
 * @deprecated
 */
function readData(guildId, conFile, data) {
    const conf = readFile(guildId, conFile);
    if (!conf || !conf[data]) return null;
    else return conf[data];
}

/**
 * Writes to a json file given json data
 * WARNING : PLEASE USE writeData IF YOU WANT TO ADD DATA, THIS FUNCTION WILL DESTROY ANY CONTENT IN THE OLD FILE !
 *
 * Uses JSON.stringify and fs.writeFileSync
 * @see JSON.stringify
 * @see fs.writeFileSync
 *
 * @param path
 * The path to the file
 *
 * @param data
 * The data to write
 *
 * @return {boolean}
 * Returns if the function was able to write new data to it.
 * A wrong return value means that permissions may be missing? In any case, the error is logged.
 */
function writeJsonFile(path, data) {
    let res = false;
    const jsonString = JSON.stringify(data, null, 2);

    try {
        fs.writeFileSync(path, jsonString);
        res = true;
    }
    catch (err) {
        console.log(err);
    }

    return res;
}

/**
 * Writes given data to a json file
 *
 * Uses readJsonFile and writeJsonFile
 * @see readJsonFile
 * @see writeJsonFile
 *
 * @param guildId
 * A Twitter snowflake, that being the server's id
 *
 * @param conFile
 * The name of the json file to write to without '.json'
 *
 * @param data
 * The data to write
 *
 * @param value
 * The value of the data to write
 *
 * @return {boolean}
 * See writeJsonFile's return value
 * @see writeJsonFile
 *
 * @deprecated
 */
function writeData(guildId, conFile, data, value) {
    let toWrite;
    if (!guildId) toWrite = confFolder + conFile + '.json';
    else toWrite = confFolder + serverFolders + guildId + '/' + conFile + '.json';

    let dt = readJsonFile(toWrite);
    if (!dt) dt = {};
    dt[data] = value;

    return writeJsonFile(toWrite, dt);
}

module.exports = { confFolder, serverFolders, createDir, createJsonFile, checkExistence, checkConfigFolder, checkServerFolder, checkGuildServerFolder, readJsonFile, readFile, readData, writeJsonFile, writeData };