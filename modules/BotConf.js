/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const Logger = require('../class/Logger');
const Translator = require('../class/Translation');

// Status and Activity types usable with an url are not fetchable from the library, at least for now
const { ActivityType } = require('discord.js');
const usableStatus = ['online', 'dnd', 'idle', 'invisible'];
const usableActivityType = Object.keys(ActivityType).filter(e => e !== 'CUSTOM' && isNaN(parseInt(e)));
const usableUrlActivityType = [ 'Streaming', 'Listening', 'Watching' ];

// Storage for the logger, after it is initialized
let logger;

/**
 * Reads the configuration from the env file
 * Checks if the config is correct using checkConf
 * @see checkConf
 */
function readConfig() {

    // Create the logger, stock for quick access, and block any modifications
    Logger.createLogger();
    logger = Logger.logger;
    Object.freeze(logger);

    // Print license, at the start
    const printLicense = require('./License');
    printLicense();

    // Check errors if this is allowed
    if (process.env.CONFIG_CHECK && process.env.CONFIG_CHECK !== 'false' || typeof process.env.CONFIG_CHECK == 'undefined') {
        if (checkConf()) {
            logger.error(Translator.getString('core.envReadFailedError'));
            process.exit(3);
        }
        logger.debug(Translator.getString('core.envCheckOk'));
    }
    else {
        logger.debug(Translator.getString('core.envCheckSkip'));
    }

    // Remove some unneeded environment variables if they exist
    if (typeof process.env.CONFIG_CHECK !== 'undefined') delete process.env.CONFIG_CHECK;
}

/**
 * Checks if the config is correct, in case it's not, it will end the process
 * @return {boolean}
 */
function checkConf() {
    let endProcess = false;

    // Enable logger in silent mode, necessary for mocha tests
    if (!logger) {
        process.env.LOGS = 'silent';
        Logger.createLogger();
        logger = Logger.logger;
        Object.freeze(logger);
    }

    logger.debug(Translator.getString('core.envReadCheck'));

    // Status must be set to be verified. If it is, it must be in the discord list of statuses
    if (process.env.STATUS && !usableStatus.includes(process.env.STATUS)) {
        logger.warn(Translator.getString('core.envCheckInvalidStatus'));
        logger.debug(Translator.getString('core.envCheckInvalidStatusDebug', { 'status' : process.env.STATUS }));
        endProcess = true;
    }
    else {
        logger.debug(Translator.getString('core.envCheckStatus', { 'status' : process.env.STATUS }));
    }

    // If there is a type, it must be inside the list of the known ones, else we set it to the default one
    if (process.env.ACTIVITY_TYPE && !usableActivityType.includes(process.env.ACTIVITY_TYPE)) {
        logger.warn(Translator.getString('core.envCheckInvalidActivityType'));
        logger.debug(Translator.getString('core.envCheckInvalidActivityTypeDebug', { 'activityType' : process.env.ACTIVITY_TYPE }));
        endProcess = true;
    }
    else {
        logger.debug(Translator.getString('core.envCheckActivityType', { 'activityType' : process.env.ACTIVITY_TYPE }));
    }

    // If there is an url, we have to check if the type is correct, else we set it to the default Url activity type
    if (process.env.ACTIVITY_URL && !usableUrlActivityType.includes(process.env.ACTIVITY_TYPE)) {
        logger.warn(Translator.getString('core.envCheckInvalidActivityUrl'));
        logger.debug(Translator.getString('core.envCheckInvalidActivityUrlDebug', { 'activityUrl' : process.env.ACTIVITY_URL }));
        endProcess = true;
    }
    else {
        logger.debug(Translator.getString('core.envCheckActivityUrl', { 'activityUrl' : process.env.ACTIVITY_URL }));
    }

    return endProcess;
}

/**
 * Removes the status and rich presence data
 */
function clearStatusAndRPData() {
    if (typeof process.env.STATUS !== 'undefined') delete process.env.STATUS;
    if (typeof process.env.ACTIVITY_NAME !== 'undefined') delete process.env.ACTIVITY_NAME;
    if (typeof process.env.ACTIVITY_TYPE !== 'undefined') delete process.env.ACTIVITY_TYPE;
    if (typeof process.env.ACTIVITY_URL !== 'undefined') delete process.env.ACTIVITY_URL;
    logger.debug(Translator.getString('core.envClear'));
}

module.exports = { readConfig, checkConf, clearStatusAndRPData };