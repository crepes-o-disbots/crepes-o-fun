FROM node:iron-alpine AS builder

# Add packages to the image, required by node-gyp
RUN apk add --no-cache python3 py3-setuptools make g++

WORKDIR /app
COPY . .

RUN ["npm", "i", "--omit=dev"]

# Add 'basic' database support, can be changed to only have what is needed
RUN npm i --save pg pg-hstore mysql2 mariadb


FROM node:iron-alpine
ENV NODE_ENV=production
WORKDIR /app
COPY --from=builder /app .

# PLEASE make sure the db_config.js file is prepared for your database of choice and you have migrated the model if there is any !
CMD npm start
