/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { describe } = require('mocha');

const assert = require('assert');
const BotConf = require('../modules/BotConf');

describe('Test botConf class', function() {

    // eslint-disable-next-line no-undef
    it('checkConf | Correct status | Should return false', function() {
        process.env.STATUS = 'online';
        assert(BotConf.checkConf() === false);
    });

    // eslint-disable-next-line no-undef
    it('checkConf | Wrong status | Should return true', function() {
        process.env.STATUS = 'egg';
        assert(BotConf.checkConf() === true);
    });

    // eslint-disable-next-line no-undef
    it('checkConf | Correct activity type | Should return false', function() {
        // Fix status to avoid a wrong value
        process.env.STATUS = 'online';
        process.env.ACTIVITY_TYPE = 'Listening';
        assert(BotConf.checkConf() === false);
    });

    // eslint-disable-next-line no-undef
    it('checkConf | Wrong activity type | Should return true', function() {
        process.env.ACTIVITY_TYPE = 'egg1';
        assert(BotConf.checkConf() === true);
    });

    // eslint-disable-next-line no-undef
    it('checkConf | Correct activity type with url | Should return false', function() {
        process.env.ACTIVITY_TYPE = 'Listening';
        process.env.ACTIVITY_URL = 'aRandomLink';
        assert(BotConf.checkConf() === false);
    });

    // eslint-disable-next-line no-undef
    it('checkConf | Wrong activity type with url | Should return true', function() {
        process.env.ACTIVITY_TYPE = 'egg2';
        assert(BotConf.checkConf() === true);
    });
});