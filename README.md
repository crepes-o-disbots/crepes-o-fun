# Crepes O Fun

This is a discord.js bot based on [Crepes O Core](https://gitlab.com/crepes-o-disbots/crepes-o-core) adding 'fun' elements as commands.

This bot is intended for personal use, do not expect any support.


## Installation/Usage

### Basic

1. Install [NodeJS](https://nodejs.org/) v20.9 or higher on your system

2. Launch a terminal inside the project and run `npm i`. This will install all required dependencies to run the bot.

3. Copy the [dotenv](example/dotenv) file to the root of the project, as `.env`. Insert at least your discord bot token, [available here](https://discord.com/developers/applications).

4. Choose a database system to use, it must be sql-based since we're using [Sequelize](https://sequelize.org/). See your `.env` file for that. Then run `npx sequelize-cli db:migrate` to update the database with the required tables. (Just run the command and rename `dev_database.db` to `database.db` if you don't care about that)

5. Run the bot with `npm start`

### Docker

#### Container registry

The provided images uses the included [Dockerfile](Dockerfile). If it doesn't correspond to your needs, you are free to build one for your own use.

1. Select the image you want to use from the [container registry](https://gitlab.com/crepes-o-disbots/crepes-o-fun/container_registry). If do not want to use docker compose, you can skip to the second part. Copy the [docker-compose.yml](docker-compose.yml) file. Modify it to point to your selected image, it should look like this : `registry.gitlab.com/crepes-o-disbots/crepes-o-fun:latest`

2. Copy the [dotenv](example/dotenv) file to the root of the project, as `.env`. Insert at least your discord bot token, [available here](https://discord.com/developers/applications).

3. Run the docker image. You can either use `docker run` or `docker compose` depending on your previous choice. Should you not use the 'composer' approach please make sure you give the required arguments to link the volumes. Please refer to the [docker-compose.yml](docker-compose.yml) file for that.

#### Build

1. Build the [Dockerfile](Dockerfile) into a Docker image, with `docker build -t crepes-o-fun .` for example. If you need another database system (sqlite for example), please modify the [Dockerfile](Dockerfile) accordingly. You can also use a [docker-compose.yml](docker-compose.yml) file to gain some time. You can find an example [here](docker-compose.yml).

2. Copy the [dotenv](example/dotenv) file to the root of the project, as `.env`. Insert at least your discord bot token, [available here](https://discord.com/developers/applications).

3. Launch your database of choice, enter the created docker image to run `npx sequelize-cli db:migrate`. This assumes you modified your `.env` to allow connection to your database.

4. Run the docker image. You can either use `docker run` or `docker compose`. Should you not use the 'composer' approach please make sure you give the required arguments to link the volumes. Please refer to the [docker-compose.yml](docker-compose.yml) file for that.
