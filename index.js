/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

// Libraries/Utils
const { readConfig } = require('./modules/BotConf');
const nodeCleanup = require('node-cleanup');

// Load translator first
const Translator = require('./class/Translation');

// Read env file for token and other configurations
readConfig();

// Load and connect to the database
const { databaseInstance } = require('./class/Database');
databaseInstance.createDatabaseConnector();
databaseInstance.connectToDB();

// Load available components in memory
const { getIntents, loadComponents } = require('./modules/InteractionComponents');
loadComponents();

// Create a client instance using custom class extending default client, intents are defined by our commands
const client = new (require('./class/BotClient'))({ intents: getIntents() });

// Events support
client.registerEvents();

// Login to Discord with the client's token
client.login(process.env.TOKEN)
    .then(() => {
        // Delete token from the environment variables, the bot should have it by now
        delete process.env.TOKEN;

        // Stop bot, close connection to database on stop
        nodeCleanup(function() {
            databaseInstance.db.close();
            client.destroy();
        });
    })
    .catch(err => {
        const { logger } = require('./class/Logger');
        databaseInstance.db.close();
        logger.error(Translator.getString('core.missingIntents', { 'intents' : getIntents() }));
        logger.error(err);
    });
