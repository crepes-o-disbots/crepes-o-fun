/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Component, parseNameToLower } = require('../../class/Component');
const { errorEmbed } = require('../../modules/EmbedResponses');
const { SlashCommandBuilder, GatewayIntentBits, PermissionsBitField } = require('discord.js');
const { logger } = require('../../class/Logger');
const { userOnTimeout } = require('./Egg/Common');
const Translator = require('../../class/Translation');

// Egg commands
const { destroy, description : destroyDescription } = require('./Egg/Destroy');
const { info, description : infoDescription } = require('./Egg/Info');
const { redeem, description : redeemDescription } = require('./Egg/Redeem');
const { set, description : setDescription, roleDescription : setRoleDescription, roleTypeDescription } = require('./Egg/Set');
const { stats, description : statsDescription } = require('./Egg/Stats');
const { steal, description : stealDescription, targetDescription : stealTargetDescription } = require('./Egg/Steal');
const { throwing, description : throwDescription, targetDescription : throwTargetDescription } = require('./Egg/Throw');
const { trying, description : tryDescription } = require('./Egg/Try');


const name = parseNameToLower(__filename);
const localizedDescriptions = Translator.getAllTranslations('funCmd.eggDescription');
const description = localizedDescriptions['en-US'];


const cmdBuilder = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .setDescriptionLocalizations(localizedDescriptions)
    .setDMPermission(false)
    .addSubcommand(subcommand =>
        subcommand.setName('destroy')
            .setDescription(destroyDescription['en-US'])
            .setDescriptionLocalizations(destroyDescription),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('info')
            .setDescription(infoDescription['en-US'])
            .setDescriptionLocalizations(infoDescription),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('redeem')
            .setDescription(redeemDescription['en-US'])
            .setDescriptionLocalizations(redeemDescription),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('set')
            .setDescription(setDescription['en-US'])
            .setDescriptionLocalizations(setDescription)
            .addStringOption(option => option.setName('roletype').setDescription(roleTypeDescription['en-US']).addChoices(
                { name : 'egg', value: 'egg' },
                { name : 'reward', value: 'reward' },
            ).setRequired(true))
            .addRoleOption(option => option.setName('role').setDescription(setRoleDescription['en-US']).setDescriptionLocalizations(setRoleDescription).setRequired(true)),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('stats')
            .setDescription(statsDescription['en-US'])
            .setDescriptionLocalizations(statsDescription),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('steal')
            .setDescription(stealDescription['en-US'])
            .setDescriptionLocalizations(stealDescription)
            .addUserOption(option => option.setName('target').setDescription(stealTargetDescription['en-US']).setDescriptionLocalizations(stealTargetDescription).setRequired(true)),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('throw')
            .setDescription(throwDescription['en-US'])
            .setDescriptionLocalizations(throwDescription)
            .addUserOption(option => option.setName('target').setDescription(throwTargetDescription['en-US']).setDescriptionLocalizations(throwTargetDescription).setRequired(true)),
    )
    .addSubcommand(subcommand =>
        subcommand.setName('try')
            .setDescription(tryDescription['en-US'])
            .setDescriptionLocalizations(tryDescription),
    );


async function egg(interaction) {
    // The bot may answer to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // Guild may not be available…
    if (!interaction.guild.available) {
        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.unavailableGuild', {}, lang))],
            ephemeral: true,
        });

        return;
    }

    const subcommand = interaction.options.getSubcommand();
    const ephemeralDeffer = ['info', 'set', 'stats'].includes(subcommand);

    // Indicate the subcommand used
    logger.info(Translator.getString('coreEvent.cmdSubExec', { 'user' : interaction.user.tag, 'userId' : interaction.user.id, 'subCmdName' : subcommand }));

    // We have to fetch the guild members, if we don't, some subcommands will fail
    await interaction.guild.members.fetch();

    // Handle users on timeout, ignore if using base commands
    if (!ephemeralDeffer && (await userOnTimeout(interaction))) {
        return;
    }

    // Try to defer the reply if needed
    await interaction.deferReply({ ephemeral: ephemeralDeffer });

    switch (subcommand) {

    case 'destroy':
        await destroy(interaction);
        break;

    case 'info':
        await info(interaction);
        break;

    case 'redeem':
        await redeem(interaction);
        break;

    case 'set':
        // All members cannot use this command
        if (!interaction.memberPermissions.has(PermissionsBitField.Flags.ManageRoles)) {
            await interaction.editReply({
                embeds: [ errorEmbed(interaction.client, Translator.getString('coreEvent.cmdMissingUserPermission', { 'perm' : 'MANAGE_ROLE' }, lang))],
                ephemeral: true,
            });

            return;
        }

        await set(interaction);
        break;

    case 'stats':
        await stats(interaction);
        break;

    case 'steal':
        await steal(interaction);
        break;

    case 'throw':
        await throwing(interaction);
        break;

    case 'try':
        await trying(interaction);
        break;

    default:
        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.impossibleSubCmd', {}, lang))],
            ephemeral: true,
        });
    }
}


const command = new Component({
    name: name,
    description: description,
    disabled: false,
    category: 'Fun',
    intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMembers],
    builder: cmdBuilder,
    mainFunction: egg,
    cooldown: 3,
});

module.exports = command;