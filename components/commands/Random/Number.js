/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed } = require('../../../modules/EmbedResponses');
const Translator = require('../../../class/Translation');

// Description & Options
const description = Translator.getAllTranslations('funCmd.randomNumberDescription');
const maxDescription = Translator.getAllTranslations('funCmd.randomNumberMaxDescription');
const minDescription = Translator.getAllTranslations('funCmd.randomNumberMinDescription');

/**
 * Returns a random number between min and max
 *
 * @param min
 * Minimum number attainable
 *
 * @param max
 * Maximum number attainable
 *
 * @returns {number}
 */
function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Returns a random number
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function number(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // Read given numbers
    const min = interaction.options.getNumber('min') ?? 0;
    const max = interaction.options.getNumber('max');

    // Calculate our number
    const nb = randomNumber(min, max);


    const embed = baseEmbed(interaction.client)
        .setTitle(Translator.getString('funCmd.randomEmbedTitle', {}, lang))
        .setDescription(`${nb}`)
        .setImage('https://media.tenor.com/5vo_w_jDfwgAAAAC/calculation-math.gif');

    await interaction.reply({
        embeds: [embed],
        ephemeral: true,
    });
}

module.exports = { number, description, maxDescription, minDescription };