/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Component, parseNameToLower } = require('../../class/Component');
const { errorEmbed } = require('../../modules/EmbedResponses');
const { SlashCommandBuilder, GatewayIntentBits } = require('discord.js');
const Translator = require('../../class/Translation');

// Subcommands
const { number, description : numberDescription, maxDescription, minDescription } = require('./Random/Number');
const { logger } = require('../../class/Logger');

const name = parseNameToLower(__filename);
const localizedDescriptions = Translator.getAllTranslations('funCmd.randomDescription');
const description = localizedDescriptions['en-US'];


const cmdBuilder = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .setDescriptionLocalizations(localizedDescriptions)
    .setDMPermission(false)
    .addSubcommand(subcommand =>
        subcommand.setName('number')
            .setDescription(numberDescription['en-US'])
            .setDescriptionLocalizations(numberDescription)
            .addNumberOption(option =>
                option.setName('max')
                    .setDescription(maxDescription['en-US'])
                    .setDescriptionLocalizations(maxDescription)
                    .setRequired(true),
            )
            .addNumberOption(option =>
                option.setName('min')
                    .setDescription(minDescription['en-US'])
                    .setDescriptionLocalizations(minDescription),
            ),
    );


async function random(interaction) {
    const subcommand = interaction.options.getSubcommand();

    // Indicate the subcommand used
    logger.info(Translator.getString('coreEvent.cmdSubExec', { 'user' : interaction.user.tag, 'userId' : interaction.user.id, 'subCmdName' : subcommand }));

    switch (subcommand) {

    case 'number':
        await number(interaction);
        break;

    default:
        await interaction.reply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.impossibleSubCmd', {}, interaction.locale))],
            ephemeral: true,
        });
    }
}

const command = new Component({
    name: name,
    description: description,
    disabled: false,
    category: 'Utils',
    intents: [GatewayIntentBits.Guilds],
    builder: cmdBuilder,
    mainFunction: random,
});

module.exports = command;