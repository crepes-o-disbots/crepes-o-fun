/**
 * Crepes O Core - Template for discord bots using discord.js
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { Component, parseNameToLower } = require('../../class/Component');
const { baseEmbed, errorEmbed } = require('../../modules/EmbedResponses');
const { SlashCommandBuilder, GatewayIntentBits } = require('discord.js');
const { readJsonFile } = require('../../modules/JsonFiles');
const Translator = require('../../class/Translation');

const name = parseNameToLower(__filename);
const localizedDescriptions = Translator.getAllTranslations('coreCmd.aboutDescription');
const description = localizedDescriptions['en-US'];

const cmdBuilder = new SlashCommandBuilder()
    .setName(name)
    .setDescription(description)
    .setDescriptionLocalizations(localizedDescriptions)
    .setDMPermission(false);


async function about(interaction) {
    // Read package.json, always exists
    const pkgInfo = readJsonFile('package.json');

    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // embed is different if not able to read package.json
    let embed;

    if (!pkgInfo) {
        embed = errorEmbed(interaction.client, Translator.getString('coreCmd.aboutNoBotInfo', {}, lang));
    }
    else {
        // Author, in some cases it is formatted in a special way
        let author = pkgInfo.author.name;

        // See https://nodejs.dev/en/learn/the-package-json-guide/#author for more info
        if (!author) {
            author = pkgInfo.author;
        }

        // Same with repo
        let repository = pkgInfo.repository.url;

        // See https://nodejs.dev/en/learn/the-package-json-guide/#repository for more info
        if (!repository) {
            repository = pkgInfo.repository;
        }

        // Get owner infos
        let owner = interaction.client.users.cache.get(process.env.OWNER_ID);

        // Owner may be null, because no owner id is given
        if (!owner) {
            owner = Translator.getString('coreCmd.aboutUnknownOwner', {}, lang);
        }
        else {
            owner = owner.tag;
        }

        // Add avatar as Thumbnail
        const avatarUrl = interaction.client.user.displayAvatarURL({ size: 2048 });

        embed = baseEmbed(interaction.client)
            .setTitle(Translator.getString('coreCmd.aboutEmbedTitle', {}, lang))
            .setThumbnail(avatarUrl)
            .addFields(
                { name: Translator.getString('coreCmd.aboutEmbedName', {}, lang), value: pkgInfo.name },
                { name: Translator.getString('coreCmd.aboutEmbedDescription', {}, lang), value: pkgInfo.description },
                { name: Translator.getString('coreCmd.aboutEmbedVersion', {}, lang), value: pkgInfo.version },
                { name: '\u200B', value: '\u200B' },
                { name: Translator.getString('coreCmd.aboutEmbedAuthor', {}, lang), value: author, inline: true },
                { name: Translator.getString('coreCmd.aboutEmbedLicense', {}, lang), value: pkgInfo.license, inline: true },
                { name: '\u200B', value: '\u200B', inline: true },
                { name: Translator.getString('coreCmd.aboutEmbedHomepage', {}, lang), value: pkgInfo.homepage, inline: true },
                { name: Translator.getString('coreCmd.aboutEmbedRepository', {}, lang), value: repository, inline: true },
                { name: '\u200B', value: '\u200B' },
                { name: Translator.getString('coreCmd.aboutEmbedOwner', {}, lang), value: owner },
            );
    }

    await interaction.reply({
        embeds: [embed],
        ephemeral: true,
    });
}

const command = new Component({
    name: name,
    description: description,
    disabled: false,
    category: 'Utils',
    intents: [GatewayIntentBits.Guilds],
    builder: cmdBuilder,
    mainFunction: about,
});

module.exports = command;