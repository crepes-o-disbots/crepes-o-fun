/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { errorEmbed, baseEmbed } = require('../../../modules/EmbedResponses');
const { logger } = require('../../../class/Logger');
const { EggRole, EggRedeem, verifyRole } = require('./Common');
const Translator = require('../../../class/Translation');

// Description & Options
const description = Translator.getAllTranslations('funCmd.eggSetDescription');
const roleDescription = Translator.getAllTranslations('funCmd.eggSetRoleDescription');
const roleTypeDescription = Translator.getAllTranslations('funCmd.eggSetRoleTypeDescription');

/**
 * Function setting the special role. Stores it into the database
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function set(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // Get given role
    const role = interaction.options.getRole('role');

    // Role managed by integration. Not usable
    if (role.managed) {
        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggSetRoleIntegrationError', {}, lang))],
            ephemeral: true,
        });

        return;
    }

    // Base role associated verifications
    if (await verifyRole(interaction, role) === false) {
        return;
    }

    // Answer may differ in some cases
    let embed;

    const roleType = interaction.options.getString('roletype');

    try {
        // Role cannot be the same, we have to check both tables for that
        const roleStored = await EggRole.findOne({
            where: { serverId: interaction.guildId },
        });
        const roletypeStored = await EggRedeem.findOne({
            where: { serverId: interaction.guildId },
        });


        // At least one exist, and identical to what is stored
        if ((roleStored && role.id === roleStored.role) || (roletypeStored && role.id === roletypeStored.role)) {
            embed = baseEmbed(interaction.client)
                .setTitle(Translator.getString('funCmd.eggSetRoleIdenticalEmbedTitle', {}, lang))
                .setDescription(Translator.getString('funCmd.eggSetRoleIdenticalEmbedText', {}, lang))
                .addFields({ name: Translator.getString('funCmd.eggSetRoleIdenticalEmbedField', {}, lang), value: `${role}` });

            // Answer to the user, no need to go further
            await interaction.editReply({
                embeds: [embed],
                ephemeral: true,
            });

            return;
        }
        else {
            if (roleType === 'egg') {
                // Update role if it exists
                if (roleStored) {
                    roleStored.role = role.id;
                    await roleStored.save();
                }
                else {
                    await EggRole.create({ serverId: interaction.guildId, role: role.id });
                }
            }
            else if (roletypeStored) {
                roletypeStored.role = role.id;
                await roletypeStored.save();
            }
            else {
                await EggRedeem.create({ serverId: interaction.guildId, role: role.id });
            }

            // Role has been saved at this point
            embed = baseEmbed(interaction.client)
                .setTitle(Translator.getString('funCmd.eggSetRoleDifferentEmbedTitle', {}, lang))
                .setDescription(Translator.getString('funCmd.eggSetRoleDifferentEmbedText', {}, lang))
                .addFields({ name: Translator.getString('funCmd.eggSetRoleDifferentEmbedField', {}, lang), value: `${role}` });
        }
    }
    catch (err) {
        embed = errorEmbed(interaction.client, Translator.getString('funCmd.eggSetSaveError', {}, lang));
        logger.error(Translator.getString('funCmd.eggSetSaveLogError', { 'server' : interaction.guild.name, 'serverId' : interaction.guildId, 'role' : role.name, 'roleId' : role.id }));
        logger.error(err);
    }


    await interaction.editReply({
        embeds: [embed],
        ephemeral: true,
    });
}

module.exports = { set, description, roleDescription, roleTypeDescription };