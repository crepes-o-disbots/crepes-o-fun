/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggTryUserStats } = require('./Common');
const Translator = require('../../../class/Translation');

// Description
const description = Translator.getAllTranslations('funCmd.eggStatsDescription');

/**
 * Function giving the user his stats on the 'role' chase
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function stats(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    const embed = baseEmbed(interaction.client).setTitle(Translator.getString('funCmd.eggStatsEmbedTitle', {}, lang));

    // Get the user's stats
    const [userEggTryStats, created] = await getEggTryUserStats(interaction);

    // Database error, do nothing
    if (userEggTryStats == null) {
        return;
    }

    // Not in the database
    if (created) {
        embed.setDescription(Translator.getString('funCmd.eggStatsEmbedTextNoData', {}, lang));
    }
    // In the database
    else {
        const ratio = (userEggTryStats.win === 0 || userEggTryStats.fail === 0) ? 0 : userEggTryStats.win / userEggTryStats.fail;

        embed.setFields(
            { name: Translator.getString('funCmd.eggStatsEmbedSuccess', {}, lang), value: `${userEggTryStats.win}`, inline: true },
            { name: Translator.getString('funCmd.eggStatsEmbedFailure', {}, lang), value: `${userEggTryStats.fail}`, inline: true },
            { name: Translator.getString('funCmd.eggStatsEmbedRatio', {}, lang), value: `${(ratio * 100).toFixed(5)}%`, inline: true },
        );
    }

    await interaction.editReply({
        embeds: [embed],
        ephemeral: true,
    });
}

module.exports = { stats, description };