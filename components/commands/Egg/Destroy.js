/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggRole, hasRole, getMembers } = require('./Common');
const randomizerDestroy = require('./randomizer_functions/Destroy');
const Translator = require('../../../class/Translation');

// Description
const description = Translator.getAllTranslations('funCmd.eggDestroyDescription');

/**
 * Allows the user to destroy the special role
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function destroy(interaction) {

    // Get role from database
    const role = await getEggRole(interaction);

    // No role found or user doesn't have the required role, stop there
    if (role === null || !(await hasRole(interaction, role, interaction.member.id, true))) {
        return;
    }

    // We need the guild members, some cases require to select a random one
    const members = await getMembers(interaction);
    if (members === null) {
        return;
    }

    // Embed may differ in some cases, but we need a base
    const embed = baseEmbed(interaction.client)
        .setTitle(Translator.getString('funCmd.eggDestroyEmbedTitle', { 'role' : role.name }, interaction.guildLocale));


    await randomizerDestroy({
        interaction : interaction,
        role : role,
        members : members,
        embed : embed,
        lang : interaction.guildLocale,
    });

    await interaction.editReply({
        embeds: [ embed ],
        ephemeral: false,
    });
}

module.exports = { destroy, description };