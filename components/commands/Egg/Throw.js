/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggRole, hasRole, checkUser } = require('./Common');
const randomizerThrow = require('./randomizer_functions/Throw');
const Translator = require('../../../class/Translation');

// Description & Options
const description = Translator.getAllTranslations('funCmd.eggThrowDescription');
const targetDescription = Translator.getAllTranslations('funCmd.eggThrowTargetDescription');


/**
 * Allows the user to throw the special role onto someone else
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function throwing(interaction) {
    // Get role from database
    const role = await getEggRole(interaction);

    // No role found or user doesn't have the required role, stop there
    if (role === null || !(await hasRole(interaction, role, interaction.member.id, true))) {
        return;
    }

    // Get the target
    const target = interaction.options.getUser('target');

    if (!(await checkUser(interaction, target))) {
        return;
    }


    // Embed may differ in some cases, but we need a base
    const embed = baseEmbed(interaction.client)
        .setTitle(Translator.getString('funCmd.eggThrowEmbedTitle', { 'role' : role.name }, interaction.guildLocale));


    await randomizerThrow({
        interaction : interaction,
        role : role,
        target : target,
        embed : embed,
        lang : interaction.guildLocale,
    });

    await interaction.editReply({
        embeds: [ embed ],
        ephemeral: false,
    });
}

module.exports = { throwing, description, targetDescription };