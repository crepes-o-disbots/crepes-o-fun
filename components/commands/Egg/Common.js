/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { errorEmbed } = require('../../../modules/EmbedResponses');
const { logger } = require('../../../class/Logger');
const { PermissionsBitField } = require('discord.js');
const Translator = require('../../../class/Translation');

// Database
const { databaseInstance } = require('../../../class/Database');
const EggRole = databaseInstance.db.modelManager.getModel('EggRole');
const EggRedeem = databaseInstance.db.modelManager.getModel('EggRedeem');
const EggTimeout = databaseInstance.db.modelManager.getModel('EggTimeout');
const EggTryStats = databaseInstance.db.modelManager.getModel('EggTryStats');


/**
 * Returns a random number between 0 and max
 *
 * @param max
 * Maximum number attainable
 *
 * @returns {number}
 */
function randomNumber(max) {
    return Math.floor(Math.random() * (max + 1));
}


/**
 * Returns information from the database for a given server
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<null|*>}
 */
async function getEggInfo(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    let line;
    try {
        line = await EggRole.findOne({
            where: { serverId: interaction.guildId },
        });
    }
    catch (err) {
        await interaction.editReply({
            embeds: [ errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonStatsError', {}, lang))],
            ephemeral: true,
        });

        logger.error(Translator.getString('funCmd.eggCommonStatsLogError', { 'server' : interaction.guild.name, 'serverId' : interaction.guildId }));
        logger.error(err);

        return null;
    }

    if (line === null) {
        await interaction.editReply({
            embeds: [ errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonDbRoleMissing', {}, lang))],
            ephemeral: true,
        });
    }

    return line;
}

/**
 * Returns the role from the current server.
 * Assumes that you are going to modify a user's role using the bot
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<*|null>}
 */
async function getEggRole(interaction) {
    // Store role, using discord's information
    const role = await interaction.guild.roles.fetch((await getEggInfo(interaction)).role);

    // Base role associated verifications
    if (await verifyRole(interaction, role) === false) {
        return null;
    }

    return role;
}

/**
 * Increments the correct egg stats for a given user on a server
 *
 * @param guildId
 * Server id, snowflake from discord
 *
 * @param memberId
 * Member id, snowflake from discord
 *
 * @param win
 * Defines what to increment, a win is when the user gets the special role, a loss is when they lose it or fail to get it
 *
 * @returns {Promise<void>}
 */
async function eggUserStatsIncrement(guildId, memberId, win) {
    try {
        const [userStats] = await EggTryStats.findOrCreate({
            where: { userId: memberId, serverId: guildId },
        });

        if (win) {
            await userStats.increment('win');
        }
        else {
            await userStats.increment('fail');
        }

        await userStats.save();
    }
    catch (err) {
        logger.error(Translator.getString('funCmd.eggCommonStatsIncrementLogError', { 'userId' : memberId, 'serverId' : guildId }));
        logger.error(err);
    }
}

/**
 * Returns a user's stats from the database for a given server
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<null|*>}
 */
async function getEggTryUserStats(interaction) {
    let userStats, created;
    try {
        [userStats, created] = await EggTryStats.findOrCreate({
            where: { userId: interaction.member.id, serverId: interaction.guildId },
        });
    }
    catch (err) {
        // The bot answers to a user in an ephemeral way, use their locale as reference
        const lang = interaction.locale;

        await interaction.editReply({
            embeds: [ errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonStatsError', {}, lang))],
            ephemeral: true,
        });

        logger.error(Translator.getString('funCmd.eggCommonStatsLogError', { 'server' : interaction.guild.name, 'serverId' : interaction.guildId }));
        logger.error(err);

        return null;
    }

    return [userStats, created];
}

/**
 * Checks if a given member has the required role
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @param role
 * Role to check
 *
 * @param memberId
 * Member id used for the check
 *
 * @param sendMsg
 * If an error message must be sent, saying that the user making the command must have the role
 *
 * @returns {Promise<boolean>}
 */
async function hasRole(interaction, role, memberId, sendMsg) {
    if (!role.members.has(memberId)) {
        if (sendMsg) {
            // The bot answers to a user in an ephemeral way, use their locale as reference
            const lang = interaction.locale;

            await interaction.editReply({
                embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.missingRole', { 'role' : role }, lang))],
                ephemeral: true,
            });
        }

        return false;
    }
    return true;
}

/**
 * Returns all members from a guild, except bots, system users and the user who made the command
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<unknown>}
 */
async function getMembers(interaction) {
    return await interaction.guild.members.fetch()
        .then(fetchedMembers => {
            return fetchedMembers.filter(member => !member.user.bot && !member.user.system && member.user.id !== interaction.member.id);
        })
        .catch(async err => {
            // The bot answers to a user in an ephemeral way, use their locale as reference
            const lang = interaction.locale;

            await interaction.editReply({
                embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.memberFetchFail', {}, lang))],
                ephemeral: true,
            });

            logger.error(Translator.getString('coreEvent.memberFetchLogFail', { 'server' : interaction.guild.name, 'serverId' : interaction.guildId }));
            logger.error(err);

            return null;
        });
}

/**
 * Verifies if the role and bot will work as intended
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @param role
 * Role instance
 *
 * @returns {Promise<null|boolean>}
 */
async function verifyRole(interaction, role) {
    // Bot guild member, necessary to check the role hierarchy
    const botInGuild = await interaction.guild.members.resolve(interaction.client.user);

    // Check role hierarchy, a negative number means the given role is higher than the best one the bot has. Meaning, it cannot add the role to the user.
    if (botInGuild.roles.highest.comparePositionTo(role) < 0) {
        // The bot answers to a user in an ephemeral way, use their locale as reference
        const lang = interaction.locale;

        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.roleHigherThanBot', {}, lang))],
            ephemeral: true,
        });

        return false;
    }

    // Check if the bot has it's required permission
    if (!botInGuild.permissions.has(PermissionsBitField.Flags.ManageRoles)) {
        // The bot answers to a user in an ephemeral way, use their locale as reference
        const lang = interaction.locale;

        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.rolePermMissing', { 'perm' : 'MANAGE_ROLES' }, lang))],
            ephemeral: true,
        });

        return false;
    }

    return true;
}

/**
 * Checks a given user
 * Sees if it's a bot, a system user or the interaction member itself
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @param target
 * Discord user
 *
 * @returns {Promise<boolean>}
 */
async function checkUser(interaction, target) {
    // Target cannot be a bot or system user
    if (target.bot || target.system) {
        // The bot answers to a user in an ephemeral way, use their locale as reference
        const lang = interaction.locale;

        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonWrongTarget', {}, lang))],
            ephemeral: true,
        });

        return false;
    }

    // Target cannot be the user itself
    if (target.id === interaction.member.id) {
        // The bot answers to a user in an ephemeral way, use their locale as reference
        const lang = interaction.locale;

        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonSameTargetAndUser', {}, lang))],
            ephemeral: true,
        });

        return false;
    }

    return true;
}

/**
 * Checks if the user trying to use the command is on timeout
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<boolean>}
 */
async function userOnTimeout(interaction) {
    let line;
    try {
        line = await EggTimeout.findOne({
            attributes : ['serverId', 'userId', 'expiresOn'],
            where: {
                serverId: interaction.guildId,
                userId: interaction.member.id,
            },
        });

        // Not stored, not on timeout
        if (!line) {
            return false;
        }

        const onTimeout = line.expiresOn.getTime() >= Date.now();
        if (onTimeout) {
            await interaction.reply({
                embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonOnTimeout', { 'time' : Math.round(line.expiresOn.getTime() - Date.now()) / 1000 }, interaction.locale))],
                ephemeral: true,
            });
        }
        else {
            await line.destroy();
        }
        return onTimeout;
    }
    catch (err) {
        await interaction.reply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonStatsError', {}, interaction.locale))],
            ephemeral: true,
        });

        logger.error(Translator.getString('funCmd.eggCommonStatsLogError', {
            'server': interaction.guild.name,
            'serverId': interaction.guildId,
        }));
        logger.error(err);

        return false;
    }
}

/**
 * Sets a given user on timeout
 *
 * @param guildId
 * Server id, snowflake from discord
 *
 * @param memberId
 * Member id, snowflake from discord
 *
 * @param time
 * Date object containing when the timeout ends
 *
 * @returns {Promise<void>}
 */
async function setUserOnTimeout(guildId, memberId, time) {
    await EggTimeout.create({
        serverId: guildId,
        userId : memberId,
        expiresOn : time,
    });
}

module.exports = { EggRole, EggRedeem, randomNumber, getEggInfo, getEggRole, eggUserStatsIncrement, getEggTryUserStats, hasRole, getMembers, verifyRole, checkUser, userOnTimeout, setUserOnTimeout };