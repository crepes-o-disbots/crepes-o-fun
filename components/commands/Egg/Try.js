/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { errorEmbed, baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggInfo, verifyRole, getMembers } = require('./Common');
const randomizerHasRole = require('./randomizer_functions/TryHasRole');
const randomizerNewRole = require('./randomizer_functions/TryNewRole');
const randomizerNoRole = require('./randomizer_functions/TryNoRole');
const Translator = require('../../../class/Translation');
const { logger } = require('../../../class/Logger');

// Description
const description = Translator.getAllTranslations('funCmd.eggTryDescription');

// Lock command usage if someone is using it
const lock = new Map();


/**
 * Actions to execute when the user doing the command has the role
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @param eggRoleData
 * Database information for the server
 *
 * @param role
 * Role from the server
 *
 * @param members
 * Members of the server
 *
 * @param embed
 * Embed to modify for the return value
 *
 * @returns {Promise<*>}
 */
async function selfNoRole(interaction, eggRoleData, role, members, embed) {
    // Time, necessary to know if we stop here or not
    const waitingTime = eggRoleData.waitingTime === null ? 0 : Math.floor(eggRoleData.waitingTime.getTime() / 1000);
    const nowTime = Math.floor(Date.now() / 1000);

    // Not allowed to try
    if (waitingTime !== 0 && waitingTime > nowTime) {
        embed.setTitle(Translator.getString('funCmd.eggTryWaitEmbedTitle', {}, interaction.guildLocale)).setDescription(Translator.getString('funCmd.eggTryWaitEmbedText', { 'waitTime' : waitingTime - nowTime }, interaction.guildLocale));
    }
    // Random time
    else {

        // Lock if user is using the command
        if (lock.get(interaction.guildId)) {
            await interaction.editReply({
                embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggLockWait', {}, interaction.locale))],
                ephemeral: true,
            });
            return null;
        }
        // Nobody is using it, or it isn't stored yet
        else {
            lock.set(interaction.guildId, true);
        }

        // Time that can be added to the command waiting time, 5 minutes by default (unfortunately, we need an object)
        const timeToAdd = {
            0: 60 * 5,
        };

        // Execute a random action
        randomizerNoRole({
            interaction : interaction,
            eggRoleData : eggRoleData,
            timeToAdd : timeToAdd,
            role : role,
            members : members,
            embed : embed,
            lang : interaction.guildLocale,
        }).catch(async err => {
            // In case of an error, the guild must be able to continue using the command
            lock.set(interaction.guildId, false);

            await interaction.editReply({
                embeds: [errorEmbed(interaction.client, Translator.getString('coreEvent.cmdExecErrorEmbed'))],
                ephemeral: true,
            });
            logger.error(err);
        });

        // We need to pass milliseconds as the argument, we have seconds
        eggRoleData.waitingTime = new Date(Date.now() + timeToAdd[0] * 1000);

        await eggRoleData.save();

        // No longer using it
        lock.set(interaction.guildId, false);
    }

    return embed;
}


/**
 * Function allowing the user to try and get the special role
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function trying(interaction) {

    // Get role from database
    const eggRoleData = await getEggInfo(interaction);

    // No role found, stop there
    if (eggRoleData === null) {
        return;
    }

    // Store role, using discord's information
    const role = await interaction.guild.roles.fetch(eggRoleData.role);

    // Base role associated verifications
    if (await verifyRole(interaction, role) === false) {
        return;
    }

    // We need the guild members, some cases require to select a random one
    const members = await getMembers(interaction);
    if (members === null) {
        return;
    }


    // Store embed base
    let embed = baseEmbed(interaction.client);

    // User can have the role
    if (role.members.has(interaction.member.id)) {
        await randomizerHasRole({
            interaction : interaction,
            role : role,
            members : members,
            embed : embed,
            lang : interaction.guildLocale,
        });
    }
    // Nobody has that role
    else if (role.members.size === 0) {
        embed.setTitle(Translator.getString('funCmd.eggTryFreeEmbedTitle', {}, interaction.guildLocale));
        await randomizerNewRole({
            interaction : interaction,
            role : role,
            embed : embed,
            members : members,
        });
    }
    // Someone has it
    else {
        embed = await selfNoRole(interaction, eggRoleData, role, members, embed);
        if (embed === null) {
            return;
        }
    }

    await interaction.editReply({
        embeds: [ embed ],
    });
}

module.exports = { trying, description };