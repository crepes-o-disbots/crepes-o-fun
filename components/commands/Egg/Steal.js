/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { errorEmbed, baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggRole, checkUser } = require('./Common');
const randomizerSteal = require('./randomizer_functions/Steal');
const Translator = require('../../../class/Translation');

// Description & Options
const description = Translator.getAllTranslations('funCmd.eggStealDescription');
const targetDescription = Translator.getAllTranslations('funCmd.eggStealTargetDescription');

/**
 * Allows the user to steal the special role from someone else
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function steal(interaction) {
    // Get role from database
    const role = await getEggRole(interaction);

    // No role found, stop there
    if (role === null) {
        return;
    }

    // Get the target
    const target = interaction.options.getUser('target');

    if (!(await checkUser(interaction, target))) {
        return;
    }

    // Target must have the role
    if (!role.members.has(target.id)) {
        await interaction.editReply({
            embeds: [ errorEmbed(interaction.client, Translator.getString('funCmd.eggRoleMissing', {}, interaction.locale))],
            ephemeral: true,
        });

        return;
    }

    // Embed may differ in some cases, we need a base
    const embed = baseEmbed(interaction.client);

    // User can have the role
    if (role.members.has(interaction.member.id)) {
        embed.setTitle(Translator.getString('funCmd.eggStealSameTargetEmbedTitle', { 'role' : role.name }, interaction.guildLocale))
            .setDescription(Translator.getString('funCmd.eggStealSameTargetEmbedText', {}, interaction.guildLocale))
            .setImage('https://c.tenor.com/SqD2xKy43LMAAAAC/what-why.gif');
    }
    else {
        embed.setTitle(Translator.getString('funCmd.eggStealDiffTargetEmbedTitle', { 'role' : role.name, 'user' : target.username }, interaction.guildLocale));
        await randomizerSteal({
            interaction : interaction,
            role : role,
            target : target,
            embed : embed,
            lang : interaction.guildLocale,
        });
    }

    await interaction.editReply({
        embeds: [ embed ],
        ephemeral: false,
    });
}

module.exports = { steal, description, targetDescription };