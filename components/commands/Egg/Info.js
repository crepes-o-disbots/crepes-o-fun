/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed } = require('../../../modules/EmbedResponses');
const { getEggRole } = require('./Common');
const Translator = require('../../../class/Translation');

// Description
const description = Translator.getAllTranslations('funCmd.eggInfoDescription');

/**
 * Returns to the user who has the role in question
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function info(interaction) {
    // The bot answers to a user in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // Get role from database
    const role = await getEggRole(interaction);

    // No role found, stop there
    if (role === null) {
        return;
    }

    // Store users having that role
    const users = [];
    role.members.forEach(member => {
        users.push(member);
    });
    if (users.length === 0) {
        users.push(Translator.getString('funCmd.eggInfoNobody', {}, lang));
    }

    // Give info to the user
    const embed = baseEmbed(interaction.client)
        .addFields(
            { name: Translator.getString('funCmd.eggInfoEmbedRole', {}, lang), value: `${role}` },
            { name: Translator.getString('funCmd.eggInfoEmbedUser', {}, lang), value: `${users.join(', ')}` },
        );

    await interaction.editReply({
        embeds: [embed],
        ephemeral: true,
    });
}

module.exports = { info, description };