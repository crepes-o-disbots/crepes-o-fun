/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const probability = require('probability-node');
const Translator = require('../../../../class/Translation');
const { setUserOnTimeout, eggUserStatsIncrement } = require('../Common');

// Lose the special role
const throw_lost = {
    p: '26%',
    f: async function({ interaction, role, target, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggThrowEmbedTextLost', { 'role' : role, 'target' : target }, lang))
            .setImage('https://c.tenor.com/Vqjlg5C75vUAAAAC/throwing-egg-kenan-williams.gif');
        await interaction.member.roles.remove(role.id);
    },
};

// Caught the 'role' in flight
const throw_catch = {
    p: '24%',
    f: async function({ interaction, role, target, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggThrowEmbedTextCatch', { 'role' : role, 'target' : target }, lang))
            .setImage('https://c.tenor.com/aoJ5cNaV5ucAAAAd/wii-wii-sports.gif');
        await interaction.member.roles.remove(role.id);

        interaction.guild.members.fetch(target.id)
            .then(async guildTarget => {
                // Give only if the target doesn't have the role
                if (!guildTarget.roles.cache.has(role.id)) {
                    await guildTarget.roles.add(role.id);
                    eggUserStatsIncrement(interaction.guildId, guildTarget.id, true);
                }
            });
    },
};

// Both users keep the role
const throw_dupe = {
    p: '20%',
    f: async function({ interaction, role, target, embed, lang }) {
        embed.setDescription(Translator.getString('funCmd.eggThrowEmbedTextDupe', { 'role' : role, 'target' : target }, lang))
            .setImage('https://c.tenor.com/s0q-KtG4lx8AAAAC/egg-eggs.gif');

        interaction.guild.members.fetch(target.id)
            .then(async guildTarget => {
                // Give only if the target doesn't have the role
                if (!guildTarget.roles.cache.has(role.id)) {
                    await guildTarget.roles.add(role.id);
                    eggUserStatsIncrement(interaction.guildId, guildTarget.id, true);
                }
            });
    },
};

// Magical 'role' coming back
const throw_magic = {
    p: '26%',
    f: async function({ role, target, embed, lang }) {
        embed.setDescription(Translator.getString('funCmd.eggThrowEmbedTextMagic', { 'role' : role, 'target' : target }, lang))
            .setImage('https://c.tenor.com/c1Y3SyKsC8QAAAAC/egg-egg-house.gif');
    },
};

// Three minute time out
const throw_timeout = {
    p: '4%',
    f: async function({ interaction, role, target, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        // Timeout for 3 minute
        await setUserOnTimeout(interaction.guildId, target.id, new Date(Date.now() + 3 * 60 * 1000));
        embed.setDescription(Translator.getString('funCmd.eggThrowEmbedTextTimeout', { 'role' : role, 'target' : target }, lang))
            .setImage('https://media.tenor.com/r4lMyPq5Zu0AAAAd/miripaskal-purim2018.gif');

        await interaction.member.roles.remove(role.id);
    },
};

const randomizer = new probability(throw_lost, throw_catch, throw_dupe, throw_magic, throw_timeout);

module.exports = randomizer;