/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const probability = require('probability-node');
const Translator = require('../../../../class/Translation');
const { eggUserStatsIncrement } = require('../Common');

// Normal message, no consequences
const hasrole_nothing = {
    p: '50%',
    f: function({ role, embed, lang }) {
        embed.setTitle(Translator.getString('funCmd.eggTryHasRoleNothingEmbedTitle', { 'role' : role.name }, lang))
            .setDescription(Translator.getString('funCmd.eggTryHasRoleNothingEmbedText', {}, lang));
    },
};

// Lost the 'role', someone else takes it
const hasrole_share = {
    p: '25%',
    f: async function({ interaction, role, members, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        embed.setTitle(Translator.getString('funCmd.eggTryHasRoleShareEmbedTitle', { 'role' : role.name }, lang))
            .setDescription(Translator.getString('funCmd.eggTryHasRoleShareEmbedText', {}, lang));
        await interaction.member.roles.remove(role.id);

        const giveTarget = members.size !== 0 ? members.random() : interaction.member;
        // Give only if the target doesn't have the role
        if (!giveTarget.roles.cache.has(role.id)) {
            await giveTarget.roles.add(role);
            eggUserStatsIncrement(interaction.guildId, giveTarget.id, false);
        }
    },
};

// Lost the 'role'
const hasrole_remove = {
    p: '25%',
    f: async function({ interaction, role, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setTitle(Translator.getString('funCmd.eggTryHasRoleRemoveEmbedTitle', { 'role' : role.name }, lang))
            .setDescription(Translator.getString('funCmd.eggTryHasRoleRemoveEmbedText', {}, lang));
        await interaction.member.roles.remove(role.id);
    },
};

const randomizer = new probability(hasrole_nothing, hasrole_share, hasrole_remove);

module.exports = randomizer;