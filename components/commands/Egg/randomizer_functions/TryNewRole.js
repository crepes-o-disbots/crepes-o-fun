/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2023-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const probability = require('probability-node');
const Translator = require('../../../../class/Translation');
const { eggUserStatsIncrement } = require('../Common');

// Gives the role, nothing
const newrole_give = {
    p: '50%',
    f: async function({ interaction, role, embed }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, true);

        embed.setDescription(Translator.getString('funCmd.eggTryFreeEmbedText', { 'role' : role }, interaction.guildLocale))
            .setImage('https://c.tenor.com/l1FUMmz0ENMAAAAC/saywhat-huh.gif');
        await interaction.member.roles.add(role);
    },
};

// A random user gets it
const newrole_random = {
    p: '50%',
    f: async function({ interaction, role, embed, members }) {
        const member = members.size !== 0 ? members.random() : interaction.member;
        eggUserStatsIncrement(interaction.guildId, member.id, true);
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggTryFreeRandomEmbedText', { 'role' : role }, interaction.guildLocale))
            .setImage('https://c.tenor.com/2boXZjb1BWAAAAAC/cat-kitty.gif');
        await member.roles.add(role);

    },
};

const randomizer = new probability(newrole_give, newrole_random);

module.exports = randomizer;