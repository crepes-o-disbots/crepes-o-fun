/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { randomNumber, eggUserStatsIncrement } = require('../Common');
const probability = require('probability-node');
const Translator = require('../../../../class/Translation');


// Troll and wait longer
const norole_trollWait = {
    p: '10%',
    f: async function({ interaction, timeToAdd, embed, lang }) {
        embed.setTitle(Translator.getString('funCmd.eggTryNoRoleWaitEmbedTitle', {}, lang));

        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        switch (randomNumber(5)) {

        // Wait 3 min
        case 0:
            timeToAdd[0] -= 60 * 2;
            embed.setDescription(Translator.getString('funCmd.eggTryNoRoleWaitEmbedText', { 'time' : 3 }, lang));
            break;

        // Wait 5 min
        case 1:
            embed.setDescription(Translator.getString('funCmd.eggTryNoRoleWaitEmbedText', { 'time' : 5 }, lang));
            break;

        // Wait 10 min
        case 2:
            timeToAdd[0] += 60 * 5;
            embed.setDescription(Translator.getString('funCmd.eggTryNoRoleWaitEmbedText', { 'time' : 10 }, lang));
            break;

        // Wait 15 min
        case 3:
            timeToAdd[0] += 60 * 10;
            embed.setDescription(Translator.getString('funCmd.eggTryNoRoleWaitEmbedText', { 'time' : 15 }, lang));
            break;

        // Wait 20 min
        default:
            timeToAdd[0] += 60 * 15;
            embed.setDescription(Translator.getString('funCmd.eggTryNoRoleWaitEmbedText', { 'time' : 20 }, lang));
        }

        embed.setImage('https://c.tenor.com/yz_7VcX0WjYAAAAd/cat-changing-the-clock-changing-the-time.gif');
    },
};

// Troll msg, can re-try instantly
const norole_trollRetry = {
    p: '13%',
    f: async function({ interaction, timeToAdd, role, embed, lang }) {
        timeToAdd[0] -= 60 * 5;

        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        switch (randomNumber(2)) {

        case 0:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollRetryEatEmbedTitle', {}, lang))
                .setImage('https://c.tenor.com/rkc-dJRFmKIAAAAC/new-world-frank.gif');
            break;

        case 1:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollRetryNoBadEmbedTitle', { 'role' : role.name }, lang))
                .setImage('https://c.tenor.com/2mmnEGc8U-0AAAAC/community-scolding.gif');
            break;

        default:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollRetryMissingEmbedTitle', { 'role' : role.name }, lang))
                .setImage('https://c.tenor.com/rkc-dJRFmKIAAAAC/new-world-frank.gif');
        }

        embed.setDescription(Translator.getString('funCmd.eggTryNoRoleTrollRetryEmbedText', {}, lang));
    },
};

// Troll msg, no re-try
const norole_trollNoRetry = {
    p: '10%',
    f: async function({ interaction, role, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        switch (randomNumber(3)) {

        // How to basic… Eggdition (video)
        case 1:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollHelpEmbedTitle', {}, lang))
                .setDescription('https://youtu.be/x2mQmKHgjUI')
                .setImage('https://c.tenor.com/1HF7w3JHP6IAAAAC/skeletor-ive-come-to-help.gif');
            break;

        // Cooking time
        case 2:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollCookEmbedTitle', {}, lang))
                .setDescription('https://youtu.be/Hs1ccQFnNZI')
                .setImage('https://c.tenor.com/DSq4mzcbFksAAAAd/swedish-chef-cooking.gif');
            break;

        default:
            embed.setTitle(Translator.getString('funCmd.eggTryNoRoleTrollFakeEmbedTitle', { 'role' : role.name }, lang))
                .setDescription(Translator.getString('funCmd.eggTryNoRoleTrollFakeEmbedText', lang))
                .setImage('https://c.tenor.com/Wcfyfh9Ct9YAAAAC/penguin-fall.gif');
        }
    },
};

// Egg to someone else… or not
const norole_tryshare = {
    p: '10%',
    f: async function({ interaction, role, members, embed, lang }) {

        // Both situations are a fail
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        const randNb = randomNumber(2);

        if (randNb === 1) {
            const giveTarget = members.size !== 0 ? members.random() : interaction.member;

            embed.setTitle(Translator.getString('funCmd.eggTryShareGiveEmbedTitle', { 'role' : role.name }, lang))
                .setDescription(Translator.getString('funCmd.eggTryShareGiveEmbedText', { 'user' : giveTarget }, lang))
                .setImage('https://c.tenor.com/XlNBUOjff-oAAAAd/happy-easter-chicken.gif');
            await role.members.random().roles.remove(role);

            // Give only if the target doesn't have the role
            if (!giveTarget.roles.cache.has(role.id)) {
                await giveTarget.roles.add(role);
            }
        }
        else {
            embed.setTitle(Translator.getString('funCmd.eggTryShareBreakEmbedTitle', { 'role' : role.name }, lang))
                .setDescription(Translator.getString('funCmd.eggTryShareBreakEmbedText', {}, lang))
                .setImage('https://c.tenor.com/rpcjY1QhzsQAAAAd/barney-rubble-betty-rubble.gif');
            await role.members.random().roles.remove(role);
        }
    },
};

// 'Steal'
const norole_steal = {
    p: '9%',
    f: async function({ interaction, timeToAdd, role, embed, lang }) {
        const stealTarget = role.members.random();

        eggUserStatsIncrement(interaction.guildId, interaction.member.id, true);
        eggUserStatsIncrement(interaction.guildId, stealTarget.id, false);

        embed.setTitle(Translator.getString('funCmd.eggTryStealEmbedTitle', { 'role' : role.name, 'user' : stealTarget.user.username }, lang))
            .setDescription(Translator.getString('funCmd.eggTryStealEmbedText', {}, lang))
            .setImage('https://c.tenor.com/aLr2Y8Ajx_cAAAAC/mine-now-steal.gif');
        await stealTarget.roles.remove(role);
        await interaction.member.roles.add(role);

        timeToAdd[0] += 60 * 10;
    },
};

// Egg dupe
const norole_dupe = {
    p: '8%',
    f: async function({ interaction, role, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, true);
        embed.setTitle(Translator.getString('funCmd.eggTryDupeEmbedTitle', { 'role' : role.name }, lang))
            .setDescription(Translator.getString('funCmd.eggTryDupeEmbedText', {}, lang))
            .setImage('https://c.tenor.com/RmiE2FnnvbEAAAAd/duplicate-soran-dussaigne.gif');
        await interaction.member.roles.add(role);
    },
};

// Egg nuke
const norole_nuke = {
    p: '5%',
    f: async function({ role, embed, lang }) {
        embed.setTitle(Translator.getString('funCmd.eggTryNukeEmbedTitle', { 'role' : role.name }, lang))
            .setDescription(Translator.getString('funCmd.eggTryNukeEmbedText', {}, lang))
            .setImage('https://c.tenor.com/giN2CZ60D70AAAAC/explosion-mushroom-cloud.gif');

        role.members.forEach(async member => {
            await member.roles.remove(role);
        });
    },
};

// Random
const norole_defaultRandom = {
    p: '35%',
    f: async function({ interaction, role, members, embed, lang }) {
        const randomTarget = members.size !== 0 ? members.random() : interaction.member;

        if (randomTarget.roles.cache.has(role.id)) {
            eggUserStatsIncrement(interaction.guildId, interaction.member.id, true);
            eggUserStatsIncrement(interaction.guildId, randomTarget.id, false);
            embed.setTitle(Translator.getString('funCmd.eggTryDefaultEmbedTitle', { 'role' : role.name }, lang))
                .setDescription(Translator.getString('funCmd.eggTryDefaultEmbedText', { 'user' : randomTarget }, lang))
                .setImage('https://c.tenor.com/92G1szGkb5IAAAAd/travis-fran-healy.gif');
            await randomTarget.roles.remove(role);
            await interaction.member.roles.add(role);
        }
        else {
            eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
            embed.setTitle(Translator.getString('funCmd.eggTryDefaultFailEmbedTitle', { 'user' : randomTarget.user.username }, lang))
                .setDescription(Translator.getString('funCmd.eggTryDefaultFailEmbedText', { 'role' : role.name }, lang))
                .setImage('https://c.tenor.com/qIRZiwT0p_YAAAAC/bad-luck-host.gif');
        }
    },
};

const randomizer = new probability(norole_trollWait, norole_trollRetry, norole_trollNoRetry, norole_tryshare, norole_steal, norole_dupe, norole_nuke, norole_defaultRandom);

module.exports = randomizer;
