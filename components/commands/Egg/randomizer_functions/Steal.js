/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const probability = require('probability-node');
const Translator = require('../../../../class/Translation');
const { setUserOnTimeout, eggUserStatsIncrement } = require('../Common');

// Steal success
const steal_success = {
    p: '15%',
    f: async function({ interaction, role, target, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, true);
        embed.setDescription(Translator.getString('funCmd.eggStealDiffTargetEmbedTextSuccess', {}, lang))
            .setImage('https://c.tenor.com/aLr2Y8Ajx_cAAAAC/mine-now-steal.gif');
        interaction.guild.members.fetch(target.id)
            .then(async guildTarget => {
                await guildTarget.roles.remove(role);
                eggUserStatsIncrement(interaction.guildId, guildTarget.id, false);
            });
        await interaction.member.roles.add(role);
    },
};

// Steal fail
const steal_failed = {
    p: '40%',
    f: async function({ interaction, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggStealDiffTargetEmbedTextFailed', {}, lang))
            .setImage('https://c.tenor.com/9T0uxnjnS-UAAAAC/epic-embed-fail-epic-embed-failure.gif');
    },
};

// Steal destroyed
const steal_destroyed = {
    p: '5%',
    f: async function({ interaction, role, target, embed, lang }) {
        embed.setDescription(Translator.getString('funCmd.eggStealDescription', {}, lang))
            .setImage('https://c.tenor.com/B8r7AoJNnqgAAAAd/golan-glory-bowl.gif');
        interaction.guild.members.fetch(target.id)
            .then(async guildTarget => {
                await guildTarget.roles.remove(role);
            });
    },
};

// Police got to you
const destroy_timeout = {
    p: '40%',
    f: async function({ interaction, role, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);

        // Timeout for 5 minute
        await setUserOnTimeout(interaction.guildId, interaction.member.id, new Date(Date.now() + 5 * 60 * 1000));
        embed.setDescription(Translator.getString('funCmd.eggStealDiffTargetEmbedTextTimeout', { 'role' : role }, lang))
            .setImage('https://c.tenor.com/zgftS54ny-IAAAAC/got-caught-pinned-down.gif');
    },
};

const randomizer = new probability(steal_success, steal_failed, steal_destroyed, destroy_timeout);

module.exports = randomizer;
