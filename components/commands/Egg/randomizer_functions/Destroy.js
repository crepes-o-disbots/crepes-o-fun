/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const probability = require('probability-node');
const { randomNumber, eggUserStatsIncrement } = require('../Common');
const Translator = require('../../../../class/Translation');

// Egg destroyed
const destroy_success = {
    p: '40%',
    f: async function({ interaction, role, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggDestroyEmbedTextSuccess', {}, lang))
            .setImage('https://c.tenor.com/nv54mDIlrzMAAAAC/egg.gif');
        await interaction.member.roles.remove(role.id);
    },
};

// Egg resisted, failed
const destroy_failed = {
    p: '20%',
    f: async function({ role, embed, lang }) {
        embed.setDescription(Translator.getString('funCmd.eggDestroyEmbedTextFailed', { 'role' : role }, lang))
            .setImage('https://c.tenor.com/aIaPN3U3w_EAAAAd/weird-cursed.gif');
    },
};

// Destroy every egg
const destroy_rampage = {
    p: '10%',
    f: async function({ role, embed }) {
        embed.setDescription(`You went on a ${role} rampage ! Everyone lost their ${role}`).setImage('https://c.tenor.com/rd8JY0dVEkoAAAAd/destruction-arcade-game.gif');
        role.members.forEach(async member => {
            await member.roles.remove(role);
        });
    },
};

// Egg shower (dupe)
const destroy_dupe = {
    p: '30%',
    f: async function({ interaction, role, members, embed, lang }) {
        eggUserStatsIncrement(interaction.guildId, interaction.member.id, false);
        embed.setDescription(Translator.getString('funCmd.eggDestroyEmbedTextDupe', { 'role' : role }, lang))
            .setImage('https://c.tenor.com/My0Scx2Vn4IAAAAC/egg-sac-dr-robotnik.gif');

        const randomNb = members.size !== 0 ? randomNumber(3) + 1 : 1;

        // Random user got the special role
        for (let i = 0; i < randomNb; i++) {
            const member = members.size !== 0 ? members.random() : interaction.member;
            member.roles.add(role);
            eggUserStatsIncrement(interaction.guildId, member.id, true);
        }

        await interaction.member.roles.remove(role.id);
    },
};

const randomizer = new probability(destroy_success, destroy_failed, destroy_rampage, destroy_dupe);

module.exports = randomizer;
