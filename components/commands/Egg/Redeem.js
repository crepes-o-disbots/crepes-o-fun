/**
 * Crepes O Fun - Discord js bot, centered around 'fun' elements
 * Copyright (C) 2022-2024 Creerio
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const { baseEmbed, errorEmbed } = require('../../../modules/EmbedResponses');
const { EggRedeem, verifyRole, hasRole, getEggRole } = require('./Common');
const Translator = require('../../../class/Translation');

// Description
const description = Translator.getAllTranslations('funCmd.eggRewardDescription');

// Lock command usage if someone is using it
const lock = new Map();

/**
 * Redeems the reward role for the current user
 *
 * @param interaction
 * Base discord interaction
 *
 * @param roleData
 * Reward role data
 *
 * @param eggRole
 * Role used by the server
 *
 * @param lang
 * Server locale
 *
 * @returns {Promise<EmbedBuilder|null>}
 */
async function redeemForUser(interaction, roleData, eggRole, lang) {
    // Fetch role to be used
    const role = await interaction.guild.roles.fetch(roleData.role);

    // Base role associated verifications
    if (await verifyRole(interaction, role) === false) {
        return null;
    }

    // Remove reward role from previous owners, except if they will have it again
    await role.members.forEach(member => {
        member.roles.remove(role);
    });

    const users = [];
    eggRole.members.forEach(member => {
        member.roles.add(role);
        member.roles.remove(eggRole);
        users.push(member);
    });


    // Store redeem time
    roleData.redeemTime = new Date(Date.now());
    await roleData.save();

    // Give info to the user
    return baseEmbed(interaction.client)
        .setTitle(Translator.getString('funCmd.eggRewardEmbedTitle'))
        .setDescription(Translator.getString('funCmd.eggRewardEmbedText', { 'reward' : role, 'users' : users.join(', ') }, lang));
}


/**
 * Redeems the reward role for the user
 *
 * @param interaction
 * Interaction returned by discord
 *
 * @returns {Promise<void>}
 */
async function redeem(interaction) {
    // The bot may answer in an ephemeral way, use their locale as reference
    const lang = interaction.locale;

    // Get role from database
    const eggRole = await getEggRole(interaction);

    // No role found or user doesn't have the required role, stop there
    if (eggRole === null || !(await hasRole(interaction, eggRole, interaction.member.id, true))) {
        return;
    }

    // Get role from database
    const roleData = await EggRedeem.findOne({
        where: { serverId: interaction.guildId },
    });

    // No role found, stop there
    if (roleData === null) {
        await interaction.editReply({
            embeds: [ errorEmbed(interaction.client, Translator.getString('funCmd.eggCommonDbRoleMissing', {}, lang))],
            ephemeral: true,
        });
        lock.set(interaction.guildId, false);
        return;
    }

    // Works on next months
    if (roleData.redeemTime.getMonth() >= new Date(Date.now()).getMonth() && roleData.redeemTime.getFullYear() >= new Date(Date.now()).getFullYear()) {
        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggRewardEmbedWait', {}, interaction.locale))],
            ephemeral: true,
        });
        return;
    }


    // Lock if user is using the command
    if (lock.get(interaction.guildId)) {
        await interaction.editReply({
            embeds: [errorEmbed(interaction.client, Translator.getString('funCmd.eggLockWait', {}, interaction.locale))],
            ephemeral: true,
        });
        return;
    }
    // Nobody is using it, or it isn't stored yet
    else {
        lock.set(interaction.guildId, true);
    }

    const embed = await redeemForUser(interaction, roleData, eggRole, lang);
    if (embed === null) {
        return;
    }

    await interaction.editReply({
        embeds: [embed],
    });

    // No longer using it
    lock.set(interaction.guildId, false);
}

module.exports = { redeem, description };